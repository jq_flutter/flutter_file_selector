
import 'dart:developer';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'model/drop_down_model.dart';
import 'model/file_util_model.dart';
import 'page/flutter_android_page.dart';
import 'comm/expan_data.dart';
///
/// @DevTool: AndroidStudio
/// @Author: ZhongWb
/// @Date: 2020/6/7 17:04
/// @FileName: flutterfileselector
/// @FilePath: flutterfileselector.dart
/// @Description: 文件选择器
///
class FlutterSelect extends StatefulWidget {
  final Widget btn;// 按钮
  final String title;// 标题
  final List<String> fileTypeEnd;// 文件后缀
  final bool isScreen;// 默认开启筛选
  final int maxCount;// 可选最大总数 默认9个
  final List<DropDownModel> dropdownMenuItem;// 类型列表
  final ValueChanged<List<FileModelUtil>> valueChanged;// 类型回调
  FlutterSelect({
    Key key,
    this.btn,
    this.fileTypeEnd,
    this.isScreen:true,
    this.title:"文件选择",
    this.maxCount,
    this.valueChanged,
    this.dropdownMenuItem,
  }):super(key:key);
  @override
  _FlutterSelectState createState() => _FlutterSelectState();
}

class _FlutterSelectState extends State<FlutterSelect> {
  List<String> fileTypeEnd;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fileTypeEnd = widget.fileTypeEnd;
    /// todo: 查询全部
    if (fileTypeEnd == null){
      fileTypeEnd = [];
      fileTypeEnd = fileTypeEnd..addAll(imgExpanName)..addAll(musicExpanName)..addAll(rarExpanName)..addAll(videoExpanName);
    }
  }
  @override
  Widget build(BuildContext context) {
    return  InkWell(
      onTap: (){
        /// 判断平台
        if (Platform.isAndroid) {
          _getFilesAndroidPage(context);
        }else if(Platform.isIOS) {
          _getFilesIosPage();
        }
      },
      child: widget.btn ?? Text("选择文件"),
    );
  }


  /// Android平台 调用Flutter布局页
  _getFilesAndroidPage(context){
    Navigator.push( context, MaterialPageRoute( builder: (context) => FlutterFileSelector(
      title: widget.title,
      isScreen: widget.isScreen,
      maxCount:widget.maxCount,
      fileTypeEnd: fileTypeEnd,
      dropdownMenuItem: widget.dropdownMenuItem,
    ), ), ).then( (value) {
      widget.valueChanged(value);
    } );
  }

  ///  IOS平台 直接使用FilePicker插件
  _getFilesIosPage() async{

    try{
      List<FileModelUtil> list = [];
      List<String> type = [];

      /// 去除点
      fileTypeEnd.forEach((t){
        type.add(t.substring(t.lastIndexOf(".")+1,t.length));
      });

      log("当前能选的类型 ios："+type.toString());

      FilePickerResult files = await FilePicker.platform.pickFiles(
        type: FileType.custom,
        allowedExtensions: type,
      );

      if(files == null  || files.count==0 || files.files==null || files.files.length==0){
        print("无文件");
      }else{
        files.files.forEach((f){
          File fi = File(f.path);
          list.add(
              FileModelUtil(
                fileDate: fi.statSync().changed.millisecondsSinceEpoch,
                fileName: fi.resolveSymbolicLinksSync().substring(fi.resolveSymbolicLinksSync().lastIndexOf("/")+1,fi.resolveSymbolicLinksSync().length),
                filePath: f.path,
                fileSize:fi.statSync().size,
                file:fi,
              ));
        });
      }
      widget.valueChanged(list);
    }catch (e){
      print("FlutterFileSelect Error:"+e.toString());
    }

  }

}



