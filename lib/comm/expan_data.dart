// 常用音乐格式
List<String> musicExpanName = [
  ".mp3",
  ".wav",
  ".ape",
  ".ogg",
  ".flac",
  ".flv",
];

// 常用视频格式
List<String> videoExpanName = [
  ".avi",
  ".mp4",
  ".rmvb",
  ".mov",
  ".rm",
  ".flv"
];

// 常用压缩包格式
List<String> rarExpanName = [
  ".zip",
  ".rar",
  ".iso",
  ".7z",
  ".gzip",
];

// 常用图片格式
List<String> imgExpanName = [
  ".bmp",
  ".jpg",
  ".png",
  ".gif",
  ".svg",
  ".webp",
  ".jpeg",
  ".heic"
];